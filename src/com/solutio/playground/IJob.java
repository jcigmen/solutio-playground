package com.solutio.playground;

import java.util.List;

/*
/*
/* Author:
/*
/* Date of creation:
/*
/* Dates of modification:
/*
/* Modification authors:
/*
/* Original file name:
/*
/* Purpose:
/*
/* Intent: For the list of jobs
/*
/* Designation:
/*
/* Classes used:
/*
/* Constants:
/*
/* Local variables:
/*
/* Parameters:
/*
/* Date of creation:
/*
/* Purpose: For the list of jobs
*/
public interface IJob {

    List<ITask> tasks();

    public interface ITask {

        void done();

        boolean getDone();

        void setDone(boolean d);
    }
}
