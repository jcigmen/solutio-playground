package com.solutio.playground;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Payroll processor class.
 * Created by Jis on 8/9/2012.
 */
public class PayrollDude implements IPayrollProcessor {

    public static final String S_DEFAULT_NAME = "_";
    public String mName;

    // depicts the previous and current years, respectively
    public Date mYear2015;
    public Date mYear2016;

    /**
     * Default constructor.
     * @param name
     * @param year1
     * @param year2
     */
    public PayrollDude(String name, Date year1, Date year2) {
        mName = name;
        mYear2015 = year1;
        mYear2016 = year2;
    }

    /**
     * @param p
     */
    public void setPyrllNamePref(String p) {
        if (p != null) {
            mName = p;
        } else {
            mName = "" + S_DEFAULT_NAME;
        }
    }

    /**
     * @param year2015
     */
    public void setStartingYearPeriod(Date year2015) {
        this.mYear2015 = year2015;
    }

    /**
     * @param mYear2016
     */
    public void setCurrentYearPeriod(Date mYear2016) {
        this.mYear2016 = mYear2016;
    }

    /**
     * Computes the payroll.
     * @param j the jobs
     * @param w the working hours
     * @param p prices per hour??
     * @return the computed
     */
    @Override
    public double compute(List<IJob> j, int w, double p) {
        // we go through each jobs and check if they have incomplete tasks
        // if there is an incomplete task, we don't compute the salary
        double salary = 0;
        int index = 0;
        while (index < j.size()) {
            for (int indexTask = 0; indexTask < j.get(index).tasks().size(); indexTask++) {
                // is the task actually done??
                boolean getDone = j.get(index).tasks().get(indexTask).getDone();
                if (getDone) {
                    if (salary <= 150000)
                        switch (mYear2016.getMonth()) {
                            case Calendar.JANUARY:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.FEBRUARY:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.MARCH:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.APRIL:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.MAY:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.JUNE:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.JULY:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.AUGUST:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.OCTOBER:
                                salary += getCompSalEnd(w, p);
                                break;
                            case Calendar.SEPTEMBER:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.NOVEMBER:
                                salary += getCompSal(w, p);
                                break;
                            case Calendar.DECEMBER:
                                salary += getCompSal(w, p);
                                break;
                            default:
                                salary += getCompSal(w, p);
                        }
                } else {
                    // we don't compute
                }
            }
            index++;
        }
        // TODO call the taxman to set the value here
        return salary;
    }

    /**
     * @param workHours
     * @param costPerHours
     * @return
     */
    private double getCompSal(int workHours, double costPerHours) {
        // the tax computation will be after the salary computation
        return new BigDecimal(Math.abs(workHours * costPerHours)).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
    }

    private double getCompSalEnd(int workHours, double costPerHours) {
        return new BigDecimal(Math.abs(workHours * costPerHours)).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
    }
}
